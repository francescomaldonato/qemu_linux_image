#!/usr/bin/bash

echo "Hello world! Bootable Linux image via QEMU - Canonical test "

sudo apt install make gcc file bzip2 build-essential wget cpio python3-minimal unzip rsync bc qemu-kvm

git clone https://github.com/buildroot/buildroot
cd buildroot
git submodule update --init

make BR2_EXTERNAL="$(pwd)/../kernel_modules" qemu_x86_64_defconfig
echo '
BR2_PACKAGE_KERNEL_MODULE=y
BR2_ROOTFS_OVERLAY="../kernel_modules/no-overlay"
' >> .config

LD_LIBRARY_PATH=

make olddefconfig
make BR2_JLEVEL="$(nproc)" all

qemu-system-x86_64 -M pc -kernel output/images/bzImage -drive file=output/images/rootfs.ext2,if=virtio,format=raw -append 'root=/dev/vda console=ttyS0' -net nic,model=virtio -nographic -serial mon:stdio -net user
