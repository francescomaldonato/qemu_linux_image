## Description
Shell script to run in a Linux environment (tested on Ubuntu 22.04 LTS).

This shell script creates and run an AMD64 Linux filesystem image using QEMU that will print “hello world” after successful startup.
The system does not contain any user/session management or prompt for login information to access the filesystem.
The script builds the kernel from source on the host environment.
The script runs within the working directory and does not consume any other locations on the host file system.

### Run the script as an executable
The script can be run as an executable Shell script. Here there is the command line to run it:
```
./linux_qemu.sh 
```

## Source tree


- buildroot/: Buildroot 2017.02, downloaded from the git repo
- kernel_module/: external package with some modules, tracked in the current git repository
    - Config.in
    - external.mk
    - external.desc
    - Makefile
    - hello.c: hello world module
    - no-overlay/
        - etc/
            - inittab: Fix to make the shell work


## Authors and acknowledgment
francesco.maldonato97@gmail.com

## License
GNU General Public License (GPL 3.0)